.. post:: May 12, 2016
   :tags: Copyright
   :author: Maarten Vermeyen

Freedom of Panorama
===================

The Belgian parliament will soon vote on a `new law <http://www.dekamer.be/FLWB/PDF/54/1484/54K1484001.pdf>`_ expanding the fair 
use of copyrighted works. The law, if passed, would introduce the 
concept of `Freedom of Panorama <https://en.wikipedia.org/wiki/Wikipedia:Freedom_of_panorama>`_ into Belgian copyright law.

The reasoning behind the law is fairly straightforward. Everybody seems
to have an uncontrollable urge to post selfies online with famous 
landmarks in the background. Since the Belgian copyright law prohibits
the publication of copyrighted works without the consent of the
author, this is often illegal.

The proposed law would add an exception to allow for the public 
reproduction of a work of visual or architectural art 
that's permanantly placed within the public space. 
There is however one important caveat: the reproduction
must not interfere with the normal exploitation of the work.

Although the intent of the exception is very clear, the phrasing
might provide a basis for interesting discussions. What is the 
normal exploitation of a work? Was it not the intent of André Waterkeyn, 
the designer of the Atomium, to generate funds from the sale of pictures?

Moreover, the Belgian copyright law already provides an exception with
regard to the panorama. It's alright to reproduce a copyrighted work in the 
public space, provided the intent of the reproduction isn't to 
reproduce the copyrighted work itself. The problem is that a narrow 
interpretation of this clause indeed prohibits selfies, which 
leaves a lot of people vulnerable to litigation.

A `news article on deredactie.be <http://deredactie.be/cm/vrtnieuws/politiek/1.2652533>`_ , 
which contains quite a bit disinformation on the topic, exagerates the 
impact of the new law. The article 
claims it's absurd that under Belgian copyright law it is 
currently illegal to take a picture of the Atomium. 
This is however not true. It's illegal to publish pictures of 
the Atomium, not to take them. There is also no mention of the fact that the 
reproduction may not obstruct the normal exploitation of the work.

It's good to see some 
legislative initiatives that restore the balance 
between the protection of the intersts of authors and the rights of the 
general public, however small the actual impact may be. On the other hand, 
should we realy be passing laws that encourage more selfies? 
 
