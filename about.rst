.. _about:

About the author 
================

:name: Maarten Vermeyen
:aka: Boris
:gpg: C5173AA3
:favourite colour: #DD4814

I live in Antwerp (Belgium) with my wife and two kids 
and work at Flanders Heritage (agentschap Onroerend Erfgoed) in Brussels.

I have a masters degree in archaeology from Ghent University and one in 
library and information science from the University of Antwerp.
