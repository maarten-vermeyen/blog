.. post:: Nov 14, 2015
   :tags: Statistics, Media
   :author: Maarten Vermeyen

Siblings and Significance
=========================
 
A couple of weeks ago my father shared
`an article <http://www.hln.be/hln/nl/961/Wetenschap/article/detail/2496461/2015/10/20/Oudste-kind-is-slimmer-dan-jongere-broer-of-zus.dhtml>`_ 
with my sister and me from a Flemish newspaper.
It boldly proclaimed that scientific research
had finally answered an age old question: the first sibling
is smarter than younger children from the same family!

I will not discuss the questionable parenting skills here, especially 
since I assume dad still has to do his Christmas shopping and 
I'm still hoping for something nice. Needless to say my sister,
being the eldest sibling, was more than a little enthusiastic about
this new scientific evidence.  

Great was my surprise when only a couple of days later I read a
`different article <http://www.demorgen.be/wetenschap/het-oudste-kind-is-niet-het-slimste-b42bb158/>`_ in another newspaper. It seemed science 
had finally given an answer to an age old question: the first 
sibling isn't smarter than younger children from the same family!

As you may have guessed, both of these newspaper articles were based on the
very same paper by Rohrer, Egloff and Schmukle published in PNAS (`DOI: 10.1073/pnas.1506451112 <http://dx.doi.org/10.1073/pnas.1506451112>`_). The paper
has received more attention than most scientific publications, 
both in specialised press and newspapers. This is partly due to the grand
scale of the research, as well as the general appeal of the subject matter. Sibling rivalry makes it a personal matter for all of us who have siblings.

It is strange to see how the interpretation can vary so wildly between 
different readers of a scientific study and what the newspaper articles choose to highlight. The study itself investigates the influence of birth order on personality. There are two conclusions:

 1. There is a significant difference in scores in objectively 
    measured intelligence and in self reported intelligence. This is however
    a relatively small difference of 1.5 IQ points.
 2. There is no significant effect if birth order on other personality traits\     such as extraversion, emotional stability, agreeableness, 
    conscientiousness, or imagination.

The most surprising of these results is the second conclusion, as it is widely
believed that birth order also has an impact on these personality traits. 
The first conclusion on the other hand is a confirmation of a small 
effect of birth order on objectively measured intellect, which had also 
been observed in other studies. 

The fact that both articles choose to translate these conclusions 
in terms of 'being smarter' shows how difficult it can be to explain a 
scientific study to a layperson. Statistical significance is almost always
interpreted as an important and meaningful difference. The fact is that a statistically significant difference can be very small. In this case it's probably to small to even notice in real life situations.   

In my opinion neither newspaper article sketches a convincing picture
of the study. I wonder if the difference in interpretation can be explained by one journalist being first-born and the other being a second son...
