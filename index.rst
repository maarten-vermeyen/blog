
.. Blog index file, created by `ablog start` on Tue Nov 10 06:54:44 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Maarten's Blog
==============

This blog is about me and my interests (more about
my interests than about me). These include information science, heritage
data analysis, technology, Open Data, Privacy issues and copyright.

Disclaimer: topics may vary...

Find out more about me here: :ref:`about`

.. postlist:: 5
   :excerpts:


.. `toctree` directive, below, contains list of non-post `.rst` files.
   This is how they appear in Navigation sidebar. Note that directive
   also contains `:hidden:` option so that it is not included inside the page.

   Posts are excluded from this directive so that they aren't double listed
   in the sidebar both under Navigation and Recent Posts.

.. toctree::
   :hidden:

   about.rst

