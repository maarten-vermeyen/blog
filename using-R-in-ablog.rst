.. post:: Apr 17, 2016
   :tags: R, Python, Sphinx, ABlog
   :author: Maarten Vermeyen
   :image: 1

Using R in ABlog
================

I'm a big fan of R, but the one thing I've always
missed is `Sphinx <http://www.sphinx-doc.org>`_. Sphinx is one 
of the most popular documentation tools and originates 
from within the Python community. It can be used to build almost 
any structured text document and
has tons of useful extensions. Even this blog is written with 
the Sphinx extension `ABlog <http://ablog.readthedocs.org/>`_.

In this post, I'll show how you can easily integrate all 
the statistical goodness of R in your Sphinx documents.

Restructured Text vs Markdown: What's in a name
-----------------------------------------------

Sphinx uses reStructuredText instead of Markdown, which seems
more popular in most non-Python communities.

The names reflects the different approaches taken by the two 
projects. Markdown tries to provide the users with a markup 
language that enables them to write plain text documents that are 
as close as possible to the intended visual appearance. It's easy
to learn and use and if you've ever taken notes in a plain text
editor, chances are you may have even unknowingly written 
some Markdown.

ReStructuredText on the other hand emphasises document structure.
It's designed to be extensible and has more features to accommodate 
a wide range of elements commonly used in documents. As such it 
provides many semantic directives which are lacking 
in Markdown. It is however more difficult to master than Markdown.

It's ease of use and it's prevalence due to the adoption by 
GitHub and others has made markdown the new markup language of choice 
within the R community. Packages such as `R Markdown <http://rmarkdown.rstudio.com/>`_ and `knitr <http://yihui.name/knitr/>`_ have enabled 
users to easily plug in some R code into documents.

One of the output formats supported by knitr is reStructured Text. By now
you probably see where I'm going with this. We'll be using knitr to generate 
reStructuredText and feed it to Sphinx to create this very blog post!

Generating rst files
--------------------

In order to generate rst files we first write an Rrst file.
This is  a file containing mixture of normal reStructured text and
chuncks of executable R code. Knitr evaluates the R code and replaces 
it with appropriate reStructuredText. The result is a file which 
can be used by Sphinx.

The following Python scripts looks for Rrst files and calls R to transform 
them into rst files. It can be run manually or integrated in to the Sphinx 
build process.

.. code:: python

   import os
   from glob import glob
   import rpy2.robjects as robjects

   rrst_files = glob('./Rrst/*.Rrst')

   for pathname in rrst_files:
       basename = os.path.splitext(os.path.basename(pathname))[0]
       outputfile = os.path.join('.', basename + '.rst')
       if os.path.exists(outputfile):
           os.remove(outputfile)

       rlibrary = robjects.r['library']
       rlibrary('knitr')
       rknit = robjects.r['knit']
       rknit(pathname, output=outputfile)

An example: Price index in Belgium
----------------------------------

The code fore the following graph is a part of this
posts Rrst file, which can be found `here <https://github.com/maarten-vermeyen/blog/blob/master/Rrst/using-R-in-ablog.Rrst>`_.


.. figure:: figure/rrst_example-1.png
    :alt: 

    

It depicts the evolution of the consumer price index
in Belgium starting from 1920 (`Open Data released by statbel <http://statbel.fgov.be/en/statistics/opendata/datasets/prices/>`_). The blue band represents a period for which no data is available due to the 
Second World War. 

So there you have it. All the statistical prowess of R integrated neatly
in Sphinx documents.  
