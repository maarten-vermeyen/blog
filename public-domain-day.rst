.. post:: Jan 1, 2017
   :tags: Copyright
   :author: Maarten Vermeyen
   :image: 1

Public Domain Day 2017
======================

.. figure:: _static/PD-icon.svg
   :align: center
   :width: 55%

Rejoice, Public Domain Day is upon us. Every year on January 1st we 
have a chance to celebrate the fact that some amazing cultural 
contributions of artists and writers enter the public domain.

The last public domain day caused quite a stir, with both the works 
of Adolf Hitler and Anna Frank becoming free to redistribute. There 
may be less controversy this year, but the list is both rich and 
interesting. Here is my top 3:

H.G. Wells
  Sci-Fi founder and writer of **The Time Machine** and **War of the Worlds**

Johan Huizinga
  One of the most famous Dutch historians of the 20th century, author of **Herfsttij der Middeleeuwen** (**The Autumn of the Middle Ages**). 
  
John Maynard Keynes
  Keynes needs no introduction. His economic ideas have had a tremendous 
  impact on 20th century economic policies worldwide.

I hope you will enjoy reading some of their work in 2017!

