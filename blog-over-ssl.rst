.. post:: Oct 24, 2016
   :tags: encryption, security 
   :author: Maarten Vermeyen
   :image: 1

Blog Over SSL
=============

Today I moved my blog from GitHub pages to my own server in order
to offer secure communication over SSL. You must be wondering why 
I would feel the need to offer a statically generated blog over HTTPS. And 
while it is true that my blog doesn't actually need encryption, I have several reasons for doing so:

 #. It's the geeky thing to do.
 #. I've grown accustomed to visiting sites using HTTPS and without the 'S' I feel kinda naked.
 #. I've had half an hour of spare time.
 #. It's Free and deceivingly easy thanks to `Let's encrypt <https://letsencrypt.org>`_

Let's encrypt
-------------

Let's encrypt is a certificate authority (CA) run by the Internet Security Research Group, a public benefit organisation. Among others its sponsered by the likes of the Linux Foundation, the Mozilla Foundation, Cisco, HP and the Electronic Frontier Foundation.

Their goal is to make HTTPS available for the masses. They do this  not only by providing free (as in beer) certificates, but also by fully automating the process of requesting and setting up the certificates. Their software for doing this is also free (as in freedom) and is ridiculously easy to use. The result is idiot proof, domain validated SSL security.

Yes indeed, since the launch of Let's Encrypt in april 2016 there are no
more excuses. Here is my `SSL report <https://www.ssllabs.com/ssltest/analyze.html?d=maarten.vermeyen.xyz>`_, show me yours...

.. figure:: _static/ssl-report.png

   SSL Report from Auqlys SSL Labs
