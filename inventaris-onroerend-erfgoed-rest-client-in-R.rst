.. post:: Feb 13, 2016
   :tags: R, REST, OpenData
   :author: Maarten Vermeyen

Using REST-services to Download Heritage Data in R
==================================================
 
A few months ago I wrote `a post <http://maarten.vermeyen.xyz/inventaris-onroerend-erfgoed-rest-client/>`_ about using Python to consume
the RESTful services provided by 
`https://inventaris.onroerenderfgoed.be <https://inventaris.onroerenderfgoed.be>`_.
Today I'll give an example application in R.

The client
----------

Like with the python client, I chose to implement the client using
an object oriented style. R supports multiple object oriented systems and
I've decided to use the `R6 object oriented system <https://cran.r-project.org/web/packages/R6/vignettes/Introduction.html>`_
because it's more similar to object oriented systems in other programming languages. I hope this
makes the example easier to understand for people who are not familiar with R. It would however
be trivial to add an S3 interface to the client.

I also use the excellent `jsonlite <https://cran.r-project.org/web/packages/jsonlite/vignettes/json-aaquickstart.html>`_ 
package to do most of the heavy lifting of parsing the json to R objects.

Instead of downloading the data and saving it to a csv file like the python example does, it returns an R dataframe. Enough talk,
here's the code.

.. raw:: html

   <script src="https://gist.github.com/maarten-vermeyen/ea1df518c54595e2a3b4.js"></script>




