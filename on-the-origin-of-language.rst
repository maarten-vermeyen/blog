.. post:: Oct 24, 2016
   :tags: Information Retrieval, Vector Space Model
   :author: Maarten Vermeyen

On the Origin of Language
=========================

For many decades linguists, neuroscientists, anthropologists 
and evolutionary biologists have been searching for the origin 
of language. The answer to this age old question has eluded us 
for centuries, but it turns out the answer was right there in
front of us all along...

Defining language
-----------------

First of all we need to define language. Let us see what 
`Wikipedia <https://en.wikipedia.org/wiki/Language>`_ 
has to say on the subject.

.. epigraph::

   Language is the ability to acquire and use complex systems 
   of communication, particularly the human ability to do so, 
   and a language is any specific example of such a system. 

   -- `Wikipedia <https://en.wikipedia.org/wiki/Language>`_

This brief and admittedly very general definition of language
makes a distinction between the human ability to use language
and specific languages. Ferdinand de Saussure, the founding 
father of modern linguistics made a further distinction between 
*Langage*, referring to the general concept of language and 
*un langue* being a specific instantiation of a language. 
Furthermore he defined *Parole* for any and all of the manifestation
of a *Langue*, e.g.: a letter, a speech, or even a blog post.

*Langage* is by definition a very abstract concept, but even any given 
*langue* is more abstract than most realise. In our day to day life we'll
only ever come in contact with *Paroles*. We'll read English texts or 
hear English speech, but we'll never experience the entirety of the English language.

So in order to investigate the origin of language we first need to
establish the relationship between *Langue* and *Parole*. When we first 
hear or read a specific text, we classify it as part of a language. Over 
time we know more of the vocabulary and we develop a feeling for the 
valid syntactic constructs of the language in question. Nobody can ever truly
*know* an entire language, since it's neigh impossible to learn every single word. Moreover, new words are added constantly and meanings of existing words 
shift over time. We therefore need to make a leap from the concrete examples 
of the *Parole* to the abstract *Langue*:

  **A Langue is the infinite set of all possible phrases and sentences, texts
  or speeches that have ever been, will ever be or could ever have 
  been instantiated.**

I must admit , there hasn't been much research into non-existing
texts or audio recordings. But fortunately the study of existing texts, and
specifically the field of information retrieval offers us a good
place to start our quest.

Information Retrieval and the Vector Space Model
------------------------------------------------

Information retrieval is a field of study which investigates
ways to find relevant information in a set of resources or documents. 
The idea here is to return a (ranked) subset of resources for a 
given information need. If this is too abstract, think of things such as 
internet search engines, online retailers like Amazon or library catalogs.

.. admonition:: For Young People
   :class: admonition note 

   A library is a place where people can borrow *books*. A book is an 
   analog device in which information is stored. It uses a text representation
   which is depicted on paper using ink. A book can hold many pieces of paper 
   in a given order. The following instructional video might clarify 
   things a bit.

   .. raw:: html

      <iframe width="560" height="315" src="https://www.youtube.com/embed/pQHX-SjgQvQ" frameborder="0" allowfullscreen></iframe>

The vector space model for information retrieval tackles the problem
of finding relevant documents by representing documents and queries as 
vectors in a vector space. The dimensions of this space are the words 
contained by the documents. In its most simple form, a document is represented as a binary vector. For each dimension (word) this vector either holds 
the value one if the word is present in the document or a zero if 
the text doesn't contain this word. In this way every document is mapped to a 
point in the vector space. The information need is represented in the same way, a query being a kind of micro-document. The cosine of the angle between 
a document vector and the query vector is a measure for their similarity. If the cosine is one, their vector representations are colinear (in this simple case they are in fact identical) and in if the cosine equals zero, none of the words match and the two vectors are perpendicular.

.. figure:: _static/vector_space.jpg
   :align: center

   Vector Space Model by Riclas, `contributed to Wikipedia <https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Vector_space_model.jpg/250px-Vector_space_model.jpg>`_

Although there aren't many practical use cases, the model itself can be abstracted to the level of sentences. In this case the document vector would contain
a one if a given sentence was present and a zero otherwise. Going one step further, we could define the dimensions as *paroles*. If the dimensions are all possible paroles, the vector space itself defines a *langue*. Taking another leap, we could define a vector space where the dimensions are *langues*, the vector space containing all languages in the world.

The origin of language
======================

Now that we have defined a vector space model for language, the answer of our quest becomes self evident. The origin of language, of any given language or in fact of any parole is the null vector, which is the origin of the vector space. The act of being silent turns out to be the origin of language. Think about any conversation you've ever had, and you'll notice they all started out with silence or as we say in Dutch:

.. epigraph:: 
   
   *De oorsprong van de taal is het zwijgen.*

.. warning::

   This blog post is not meant to be a serious attempt to explain
   the origin of language. If anything, it is a sleight of words, a critique
   on the post-modernist practice of importing concepts from other fields
   without any regard of their actual meaning.
