.. post:: Nov 11, 2015
   :tags: Python, REST, OpenData
   :author: Maarten Vermeyen

Using REST-services to Download Heritage Data
=============================================
 
Every now and then at work we get asked where 
people can obtain a copy of the data available on 
https://inventaris.onroerenderfgoed.be (in dutch). 
The data model is quite complex and the database 
structure is ever changing as the application 
evolves and is adapted to suit new buisiness needs. 
As a consequence, we usualy don't hand over 
database copies. We advertise using our RESTfull/RESTish
sevice layer instead.


What is this dataset you speak of?
----------------------------------

The inventory of immovable heritage 
contains data on heritage sites in Flanders. 
It is a very rich dataset consisting of structured data, 
as well as a textual description, 
pictures and geographical locations. More background
information on the dataset can be found in a recent 
paper by `Van Daele, Meganck and Mortier <http://www.researchgate.net/publication/281856635_Data-driven_systems_and_system-driven_data_the_story_of_the_Flanders_Heritage_Inventory_%281995-2015%29>`_.

The data is released under both a creative
commons attribution licence and an Open Data licence from 
the Flemish Government. There is an important exception. Some
of the pictures were contributed by third parties and can't
be released under an Open Data licence. Fortunatly these can be 
easily identified by the copyright notice.

Consuming the services
----------------------

The service allows you to ``GET`` a JSON-representation of
a single object or to search for items and get 
paginated results. I won't go into the details of
the model or the search capabilities in this post, so let's
dive right in.

The following script defines a client for the search service,
which is capable of running a query and saving the results in 
a csv file.

.. raw:: html

   <script src="https://gist.github.com/maarten-vermeyen/005c27e3e03f8ebfb9fc.js"></script>

The script works in a pretty basic fashion. It defines a 
client class `OepsRestClient`, wich has a method `get_query_results`.
You can pas in a dictionary of query parameters. 
These are URL encoded and
passed to the service endpoint with an accept header.

The service responds with a paginated JSON representation of the resultset,
containing URL's to the matched objects. This looks something like this.

.. code:: javascript

   {
    "identifier": "id",
    "label": "omschrijving",
    "totalResults": 107,
    "startIndex": 0,
    "itemsPerPage": 25,
    "links":
    {
        "self":
        {
            "type": "application/json",
            "href": "https://inventaris.onroerenderfgoed.be/erfgoed/node/.json?query=dibe_relict+schilde"
        },
        "first":
        {
            "type": "application/json",
            "href": "https://inventaris.onroerenderfgoed.be/erfgoed/node/.json?query=dibe_relict+schilde&pagina=1"
        },
        "next":
        {
            "type": "application/json",
            "href": "https://inventaris.onroerenderfgoed.be/erfgoed/node/.json?query=dibe_relict+schilde&pagina=2"
        },
        "last":
        {
            "type": "application/json",
            "href": "https://inventaris.onroerenderfgoed.be/erfgoed/node/.json?query=dibe_relict+schilde&pagina=5"
        }
    },
    "items":
    [
        {
            "id": "dibe_relict.14135",
            "links":
            [
                {
                    "type": "text/html",
                    "href": "https://inventaris.onroerenderfgoed.be/dibe/relict/14135"
                },
                {
                    "type": "application/json",
                    "href": "https://inventaris.onroerenderfgoed.be/erfgoed/node/dibe_relict.14135.json"
                }
            ],
            "type": "dibe_relict",
            "omschrijving": "Atelierwoning Albert Van Dyck",
            "primaire_foto":
            {
                "id": 155200,
                "sizes":
                {
                    "small": "https://inventaris.onroerenderfgoed.be/afbeeldingen/155200?size=small",
                    "medium": "https://inventaris.onroerenderfgoed.be/afbeeldingen/155200?size=medium"
                }
            },
            "locatie": "Oudebaan 130, Schilde (Antwerpen)",
            "korte_beschrijving": "Kleine alleenstaande woning op omhaagd, beboomd domein op hoek met Albert Van Dyckstraat. Opgetrokken naar ontwerp van Huib Hoste in 1930."
        },
   ...

The script iterates over the items in the first page of the resultset. For
each item, it fetches the complete JSON representation for the object using
the provided url:

.. code:: javascript

   {
        "id": "dibe_relict.14161",
        "type": "dibe_relict",
        "omschrijving": "Bellemondhoeve",
        "vastgesteld": true,
        "primaire_foto":
        {
            "id": 155170,
            "sizes":
            {
                "small": "https://inventaris.onroerenderfgoed.be/afbeeldingen/155170?size=small",
                "medium": "https://inventaris.onroerenderfgoed.be/afbeeldingen/155170?size=medium",
                "full": "https://inventaris.onroerenderfgoed.be/afbeeldingen/155170?size=full"
            }
        },
        "fotos":
        [
            {
                "id": 155170,
                "label": "schilde_'sgravenwezel_bellemond_022 (Onbepaald, 17-08-1976, ©Vlaamse Gemeenschap)",
                "sizes":
                {
                    "small": "https://inventaris.onroerenderfgoed.be/afbeeldingen/155170?size=small",
                    "medium": "https://inventaris.onroerenderfgoed.be/afbeeldingen/155170?size=medium",
                    "full": "https://inventaris.onroerenderfgoed.be/afbeeldingen/155170?size=full"
                }
            }
        ],
        "locatie":
        {
            "provincie": "Antwerpen",
            "gemeente": "Schilde",
            "deelgemeente": "'s Gravenwezel",
            "straat": "Bellemond",
            "omschrijving": "Bellemond 22, Schilde (Antwerpen)",
            "geometry_center":
            {
                "type": "Point",
                "coordinates":
                [
                    4.563103450877,
                    51.243863826876
                ]
            },
            "geometry":
            {
                "type": "MultiPoint",
                "coordinates":
                [
                    [
                        4.563103450877,
                        51.243863826876
                    ]
                ]
            }
        },
        "primaire_tekst":
        {
            "id": 14161,
            "titel": "Bellemondhoeve",
            "tekst": "

    Zogenaamd "Bellemondhoeve". Oorspronkelijk hoeve met losstaande bestanddelen, uit de 18de eeuw (?). Woonstalhuis (nok parallel aan de straat) en langsschuur (nok parallel aan de straat) aan noordzijde. Gelegen in beboomd domein afgesloten met rododendronstruiken.


     Reeds in het tweede kwart van de 17de eeuw vermeld circa 1950 naar ontwerp van Marc de Vooght verbouwd tot woning met onder meer aanbreng dakvensters, inrichten stallinggedeelte, nieuwe muuropeningen enzomeer.


     Woonstalhuis met noordelijke voorgevel van acht traveeën en één bouwlaag onder zadeldak (mechanische pannen) met circa 1950 aangebrachte rechth. dakvensters met trapgevels. Verankerde en begroeide baksteenbouw op gecementeerde plint. Onderkelderde tweede travee met opkamervenster (bolkozijn). Vernieuwde en nieuwe rechthoekige vensters, bol- en kruiskozijnen. Eveneens vernieuwde rechthoekige deuren; voordeur met rechthoekig bovenlicht en oorspronkelijke staldeur met sluitsteen onder vierkant bovenlicht. Omlijstingen van zandsteen met negblokken. Westelijke zijgevel met sporen van muurvlechtingen.


     Langsschuur onder schilddak (Vlaamse pannen). Versteende vakwerkbouw met gedeeltelijk behoud van de stijlen; bewaard gebint.


        VENSTERMANS J., 1874-1954 Tachtig jaar beroepsorganisatie van het bouwbedrijf te Antwerpen, Antwerpen, 1954.
    ",
            "datum": 1985,
            "auteurs":
            [
                {
                    "id": 103,
                    "type": "publieke persoon",
                    "omschrijving": "Wylleman"
                }
            ]
        },
        "typologie":
        [
            {
                "id": 71,
                "term": "boerenwoningen",
                "link":
                {
                    "type": "text/html",
                    "href": "https://inventaris.onroerenderfgoed.be/thesaurus/typologie/71"
                }
            },
            {
                "id": 1423,
                "term": "hoeven met losse bestanddelen",
                "link":
                {
                    "type": "text/html",
                    "href": "https://inventaris.onroerenderfgoed.be/thesaurus/typologie/1423"
                }
            },
            {
                "id": 381,
                "term": "schuren",
                "link":
                {
                    "type": "text/html",
                    "href": "https://inventaris.onroerenderfgoed.be/thesaurus/typologie/381"
                }
            },
            {
                "id": 411,
                "term": "stallen",
                "link":
                {
                    "type": "text/html",
                    "href": "https://inventaris.onroerenderfgoed.be/thesaurus/typologie/411"
                }
            }
        ],
        "datering":
        [
            {
                "id": 18,
                "term": "18de eeuw",
                "link":
                {
                    "type": "text/html",
                    "href": "https://inventaris.onroerenderfgoed.be/thesaurus/datering/18"
                }
            }
        ],
        "personen":
        [
            {
                "id": 1611,
                "naam": "De Vooght, Marc",
                "rol": "Ontwerper",
                "link":
                {
                    "type": "text/html",
                    "href": "https://inventaris.onroerenderfgoed.be/dibe/persoon/1611"
                }
            }
        ],
        "relaties":
        [
            {
                "id": "dibe_geheel.102081",
                "type":
                {
                    "id": "DV",
                    "omschrijving": "maakt deel uit van"
                },
                "omschrijving": "Bellemond",
                "links":
                [
                    {
                        "type": "text/html",
                        "href": "https://inventaris.onroerenderfgoed.be/dibe/geheel/102081"
                    },
                    {
                        "type": "application/json",
                        "href": "https://inventaris.onroerenderfgoed.be/erfgoed/node/dibe_geheel.102081.json"
                    }
                ]
            }
        ],
        "korte_beschrijving": "Oorspronkelijk hoeve met losstaande bestanddelen, uit de 18de eeuw (?). Woonstalhuis en langsschuur aan noordzijde. Gelegen in beboomd domein afgesloten met rododendronstruiken.",
        "actief": true,
        "systemfields":
        {
            "created_at": "2007-10-26T13:41:00+0200",
            "created_by": "verwinka",
            "updated_at": "2013-11-26T10:52:03+0100",
            "updated_by": "verwinka"
        }
    }

The client parses the returned json and extracts some of the data. It also
collects some basic statistics and writes everything to the specified csv 
file. You could change this part if you want other data in the CSV export, 
you get the gist (pun intended).

When it reaches the end of the current page, the client follows the link to the
next page an parses the result. It repeats this process until there are no more
links to follow. And that's it. Easy peasy... 



