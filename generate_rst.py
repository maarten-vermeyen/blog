import os
from glob import glob
import rpy2.robjects as robjects

# get Rrst files
rrst_files = glob('./Rrst/*.Rrst')

# knit each Rrst file to a rst file
for pathname in rrst_files:
    basename = os.path.splitext(os.path.basename(pathname))[0]
    outputfile = os.path.join('.', basename + '.rst')
    if os.path.exists(outputfile):
        os.remove(outputfile)

    rlibrary = robjects.r['library']
    rlibrary('knitr')
    rknit = robjects.r['knit']
    rknit(pathname, output=outputfile)

