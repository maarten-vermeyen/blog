.. post:: Mar 4, 2017
   :tags: Statistics
   :author: Maarten Vermeyen
   :image: 1

If I had a nickel...
====================

.. image:: _static/us_nickel_2013.png
   :width: 25%
   :align: center
 
*If I had a nickel for every time <insert event here>, I would 
be rich. But just how rich? Let us run the
numbers...*

Let's say you just graduated from Harvard, but instead of relying 
on your hard-earned degree, you find a job as a nickel picker.
If you pick up a nickel every second of your 45 year long career, 
pulling twelve hours shifts every single day you would have 
accumulated a grand total of 35,478,000 USD. The nickel picking trade 
pays well indeed.

Granted, you are now rich by any standard, but according to 
Credit Suisse's annual `Global Wealth Databook <https://www.credit-suisse.com/ch/en/about-us/research/research-institute/publications.html>`_
 there are an estimated 1,475,991 people around the world 
who have a net worth in between 10,000,000 and 50,000,000 USD. 
That's about the same amount of people Donald Trump claims attended 
his inauguration. According to the same report, there are another 140,949 
individuals who have a net worth exceeding 50,000,000 USD.

If we want to make the list of the 2000 fattest cats in the world, we
clearly have some work to do. We would have to pick about 29 nickles every
second of our 45 year long career in order to accumulate the 1 billion 
dollar mark that gets us on that list.

Greed is good. So let's take the next step. We want to be top dog, the 
wealthiest person on the planet. To beat Bill Gates, 
the current leader on the 
`scoreboard <https://en.wikipedia.org/wiki/The_World's_Billionaires#2016>`_
, we would need to pick up 2,113 nickels every second. That means 
shoveling nickels at an astounding rate of 10.565 Kg per second for twelve
hours a day, 7 days a week, all year round for 45 long years...
