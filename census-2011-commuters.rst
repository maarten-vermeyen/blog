.. post:: Feb 26, 2016
   :tags: R, OpenData, statbel
   :author: Maarten Vermeyen
   :image: 1

Census 2011 Commuter Data
=========================
 
Last week I was browsing the `Open Data portal of the 
Belgian federal government <http://data.gov.be>`_. Unfortunatly
a lot of Open Data released by governments is 
already heavily processed and aggregated to a point where you can
do little more than plot a bar chart.

Sometimes this is due to privacy concerns. A lot of data 
collected by governmental agencies are the result of interactions
with it's citizens. Because this data is directly or indirectly linked
to individuals, there is always a chance that one could deduce the identity of
some cases in the dataset. Aggregating data is probably the most common
way to deal with this issue. 

Other times it's because the data is assembled
only for the purpose of policy evaluation and monitoring. In this context
data usually comes from a wide variety of sources and it probably
wasn't collected with analysis in mind. The raw data is often dirty and has
a complex structure which makes it difficult to handle. Heavy processing
is done to weed out errors and simplify data stuctures before releasing it. 

So I usually don't get my hopes up when scavenging the net for interesting
open datasets. But when something pops up from the Directorate-general Statistics (a.k.a. `statbel <http://statbel.fgov.be/>`_), it's often worth checking out.

On the fourth of february they released a matrix containing 
data on commuters in Belgium. Since this dataset also contains 
some geographical data, I thought it would be an excellent chance to 
experiment with `Shiny <http://shiny.rstudio.com/>`_ and 
`Leaflet for R <http://rstudio.github.io/leaflet/>`_. The results of me goofing around can be found on `GitHub <https://github.com/maarten-vermeyen/census-2011-commuters>`_ and the shiny application is also deployed on `my shiny server <http://shiny.vermeyen.xyz/census-2011-commuters>`_.

.. figure:: _static/census-2011-commuters.png

   Shiny app on http://shiny.vermeyen.xyz/census-2011-commuters/ 

The app is pretty straightforward. It shows the number of inbound 
or outbound commuters for a given municipality. I'm not to happy with 
using OpenStreetMaps as a base layer here, because it detracts a bit
from the data itself. I've added the more suitable 
`GRB-grayscale <https://www.agiv.be/producten/grb>`_ as an 
alternative option, but this layer is only available for 
the Flanders region. 

Most of the work goes into loading the data, creating and reprojecting
the original dataset. Because this takes some time, the resulting inbound 
and outbound spatial dataframes are cached as 
rds-files when running the script for the first time.

I found using leaflet easy enough and the shiny integration was a breeze. 
Getting the interaction with the user input widgets and updating all the map 
elements was a bit tricky at first, but thanks to the excelent documentation
I never lost too much time messing around.
