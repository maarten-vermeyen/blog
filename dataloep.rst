.. post:: Apr 8, 2016
   :tags: OpenData, Education
   :author: Maarten Vermeyen

Dataloep Launched
=================

A couple of days ago the department of education of the Flemish government
launched `Dataloep <http://onderwijs.vlaanderen.be/nl/onderwijsstatistieken/dataloep-aan-de-slag-met-cijfers-over-onderwijs>`_ .
The application allows you to explore the number of
enrolled students from kindergarten through college.

It's always great to see reliable data published, especially
when it's on such an important topic as education. Dataloop allows
you to filter and create tables and visualisation
based on a wide variety of attributes.

The application is well documented and there are extensive
help pages describing what you're looking at. All terminology and attributes are
neatly defined and documented. Kudos!

The application itself is built with
`QlikView <http://www.qlik.com/products/qlikview>`_, a BI
tool for guided analytics. Now I'm not usually a huge fan of these
sort of products, mostly because I feel people should not
make data visualizations without fully understanding the data.
Many BI tools seem also seem to have a tendency towards less then ideal
graph types, such as pie charts, donut charts or sparklines.

These tools usually allow a predefined set of graphs to be
generated while the underlying data is changed based on user
selections. This gives a great degree of flexibility, but also
allows user to make some useless visualisations. The following
doughnut chart `won't surprise anyone <http://statbel.fgov.be/nl/statistieken/cijfers/bevolking/structuur/leeftijdgeslacht/belgie/>`_.

.. figure:: _static/high_school_students_by_gender.png

   Number of high school students by gender

Some visualisations are bit difficult to read and
can even be misleading. The next two graphs basically represent
the same data. The first represents the number of enrolled
students in each of the educational networks in Flanders (Flemish
community network, subsidised public network and the and
the subsidised free network). This is a decent visualisation
of the data, allowing the viewer to easily spot the main trends
as well as the relative size of each network.

.. figure:: _static/evolution_number_of_students.png

   Evolution of number of students by educational network

The next graph visualized the percentage of growth relative
to the previous year.

.. figure:: _static/percentage_growth.png
   
   Percentage of growth by educational network

Now a percentage is always a tricky thing. I tend to avoid plotting
percentages over time like this, because the unit represents a
different absolute amount on different places in the plot. In
this plot the unit doesn't even represent the same amount
for different categories within the same year. It's like
comparing apples, oranges and bannanas in 2011 to
sprouts, eggplants and zucchinis in 2012.

That being said, the application is a great source of information and
allows you to quickly select and visualise interesting data. You can
also download the data used for all visualisations and even assemble
your own subsets of the data to download and get started on your
own visualisations. Go check it out!

